"""JUnit XML formatter."""
from importlib.metadata import version as _version
from pathlib import Path
from typing import Optional
from xml.etree import ElementTree  # noqa: S405

from flake8.formatting import base
from flake8.style_guide import Violation
from junitparser import Failure, JUnitXml, TestCase, TestSuite

__version__ = _version("flake8-junit-xml")


class JUnitXMLFormatter(base.BaseFormatter):
    """JUnit XML formatter."""

    _report: JUnitXml
    _testsuites: dict[str, TestSuite]

    def after_init(self) -> None:
        """Initialize report."""
        self._report = JUnitXml("flake8")
        self._testsuites = {}

    def start(self) -> None:
        """Prepare file for writing."""
        if self.filename:
            path = Path(self.filename)
            path.parent.mkdir(parents=True, exist_ok=True)
            self.output_fd = open(path, "w")

    def stop(self) -> None:
        """Write report after all errors."""
        ElementTree.indent(self._report._elem)
        content = self._report.tostring().decode()
        self._write(content)
        return super().stop()

    def beginning(self, filename: str) -> None:
        """Create a test suite for each file."""
        self._testsuites[filename] = TestSuite(filename)

    def finished(self, filename: str) -> None:
        """Add the test suite to the report."""
        testsuite = self._testsuites[filename]
        self._report.add_testsuite(testsuite)

    def handle(self, error: Violation) -> None:
        """Add a test case to the test suite."""
        failure = Failure(message=self.format(error), type_=error.code)
        failure.text = self.show_source(error)

        testcase = TestCase(self.format(error))
        testcase.append(failure)

        testsuite = self._testsuites[error.filename]
        testsuite.add_testcase(testcase)

    def format(self, error: Violation) -> Optional[str]:
        """Format the error."""
        return "{filename}:{line_number}:{column_number} {code} {text}".format_map(
            error._asdict()
        )
