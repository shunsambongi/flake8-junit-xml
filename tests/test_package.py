"""Package-level tests."""
import flake8_junit_xml


def test_version() -> None:
    """Make sure version is defined."""
    assert hasattr(flake8_junit_xml, "__version__")
