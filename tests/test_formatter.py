"""Test the formatter."""
import argparse
from pathlib import Path
from typing import Callable, Optional

import pytest
from flake8.style_guide import Violation
from pytest_lazyfixture import lazy_fixture
from pytest_regressions.file_regression import FileRegressionFixture

from flake8_junit_xml import JUnitXMLFormatter


@pytest.fixture()
def output_stdout(
    capsys: pytest.CaptureFixture,
) -> tuple[Optional[str], Callable[[], str]]:
    """Fixture to make formatter write to stdout."""
    return None, lambda: capsys.readouterr().out


@pytest.fixture()
def output_file(tmp_path: Path) -> tuple[Optional[str], Callable[[], str]]:
    """Fixture to make formatter write to file."""
    path = tmp_path / "report.xml"
    return str(path), path.read_text


@pytest.mark.parametrize(
    "show_source", [True, False], ids=["with_source", "without_source"]
)
@pytest.mark.parametrize(
    "case",
    [lazy_fixture("output_stdout"), lazy_fixture("output_file")],
    ids=["stdout", "file"],
)
def test_formatter(
    case: tuple[Optional[str], Callable[[], str]],
    file_regression: FileRegressionFixture,
    show_source: bool,
) -> None:
    """Test formatter."""
    output_file, get_result = case
    filename = "test.py"
    error = Violation("TEST000", filename, 1, 2, "This is a test error", "An error\n")

    formatter = JUnitXMLFormatter(
        argparse.Namespace(output_file=output_file, tee=False, show_source=show_source)
    )
    try:
        formatter.start()
        formatter.beginning(filename)
        formatter.handle(error)
        formatter.finished(filename)
    finally:
        formatter.stop()

    file_regression.check(get_result(), extension=".xml")
