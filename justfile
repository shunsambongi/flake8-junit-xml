_default:
    @just --list

# Generate .gitignore
gitignore:
    curl -sL https://www.toptal.com/developers/gitignore/api/python >> .gitignore
